<?php

use Illuminate\Database\Migrations\Migration;

class CreateAuthorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('authors', function($t) {
            $t->increments('id');
            $t->string('name');
            $t->string('midname');
            $t->enum('lang', array('RU', 'EN', 'BY'));
            $t->enum('class', array('A', 'P', 'C'));
            $t->integer('nbook');
            $t->integer('series');
            $t->timestamps();
//            ->unique()
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('authors');
	}

}