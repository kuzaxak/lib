<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('author');
            $table->string('name');
            $table->enum('lang', array('RU', 'EN', 'BY'));
            $table->integer('series');
            $table->integer('number');
            $table->integer('year');
            $table->string('transl');
            $table->string('path'); //путь к файлу от корня библиотеки
            $table->string('fname'); //имя файла (без .zip)
            $table->integer('size');
            $table->integer('asize');
            $table->date('added');
            $table->bigInteger('crc32');
			$table->timestamps();
		});

        /**
         *
         * - id      id книги
        - author  id автора
        - name    название книги
        - lang    язык (RU/EN)
        - series  серия или 0
        - number  номер в серии или 0
        - year    год написания или 0
        - transl  переводчик
        - path    путь к файлу от корня библиотеки
        - fname   имя файла (без .zip)
        - size    размер файла в байтах
        - asize   размер архива в байтах
        - added   дата добавления/обновления книги (YYYMMDD)
        - crc32   crc32 для файла (bigint)
         *
         *
         */
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}

}
