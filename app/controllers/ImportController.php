<?php

class ImportController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return 'Import page';
	}

    public function getConvertDb($db = null)
    {
        $path = storage_path();
        $path_new = $path.'/import_db/new/';
        $path_csv = $path.'/import_db/csv/';

        $files = scandir($path_new);
        foreach($files as $file) {
            if (is_file($path_new.$file)) {
                $filesToImport[] = $path_new.$file;
            }
        }
        asort($filesToImport);

        foreach($filesToImport as $file) {
            $handle = @fopen($file, "r");

            if ($handle) {
                $bom = fread($handle, 3);
                if ($bom != "\xEF\xBB\xBF") {
                    rewind($file, 0);
                }
                while (($buffer = fgets($handle)) !== false) {
                    $buffer = utf8_decode($buffer);
                    $data = explode("\t", $buffer, 7);
                    $imei = trim($data[0]);
                    $model = trim($data[1]);


                }
                if (!feof($handle)) {
                    die('dd');
                }
                fclose($handle);

                rename($file, $path.'/'.date('d-m-Y-H-i-s', time()).".txt");
            } else {
                imeiLog($fileLog, "Error reading file '$file'", false);
                continue;
            }
        }

        return 'convert db:  '.$db.'  '.storage_path();
    }

}